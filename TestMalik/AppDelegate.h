//
//  AppDelegate.h
//  TestMalik
//
//  Created by Ronish on 3/22/16.
//  Copyright © 2016 Mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

